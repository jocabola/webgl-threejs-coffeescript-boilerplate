# CoffeeScript+Three.js Boilerplate
A boilerplate for jocabola projects.

### Instructions of use

1) Install

`make install`

2) To compile type:

`make`

3) To launch server type:

`make server`

This should open your browser automatically.