class Renderer
	constructor: ( container ) ->
		w = Math.max window.innerWidth, 960
		h = Math.max window.innerHeight, 540
		@renderer = new THREE.WebGLRenderer { antialias: true }
		@renderer.setSize w, h
		container.appendChild @renderer.domElement

		@camera = new THREE.PerspectiveCamera 45, w/h, 1, 10000

		@scene = new THREE.Scene()
		@scene.add @camera

		if window.addEventListener
			window.addEventListener 'resize', ( e ) =>
				@resize()
			, false
		else if window.attachEvent
			window.attachEvent 'resize', ( e ) =>
				@resize()
			, false

	render: () ->
		@renderer.render @scene, @camera

	resize: () ->
		w = Math.max window.innerWidth, 960
		h = Math.max window.innerHeight, 540
		@renderer.setSize w, h
		@camera.aspect = w/h
		@camera.updateProjectionMatrix()

module.exports = Renderer;