Renderer = require '../gfx/Renderer.coffee'
#glslify = require 'glslify'

class App
	constructor: ( @container ) ->
		@renderer = new Renderer @container
		@time = 0
		@oTime = Date.now()

		@renderer.scene.add new THREE.AmbientLight( 0x333333 )
		@renderer.scene.add new THREE.PointLight( 0xffffff, .7 )

		@mesh = new THREE.Mesh new THREE.BoxGeometry( 100, 100, 100 ), new THREE.MeshPhongMaterial( { color: 0x999999, specular: 0xffffff, shininess: 2 } )
		@renderer.scene.add @mesh
		@mesh.position.z = -500

		#@gui = new dat.GUI()

	update: () ->
		time = ( Date.now() - @oTime ) * .001
		dt = time - @time
		@time = time

		@mesh.rotation.y += dt
		@mesh.rotation.x -= dt
	
	render: () ->
		@renderer.render()

module.exports = App;